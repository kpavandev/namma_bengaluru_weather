package com.example.pavan.myweather;

import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;


public class MainActivity extends AppCompatActivity {
    TextView []textView = new TextView[10];
    private Spinner selectitem;
    private String addr,saddr;
    private String ip;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_main);
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        selectitem=(Spinner) findViewById(R.id.text_interested);

        selectitem.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                addr = selectitem.getSelectedItem().toString();
                ((TextView) parent.getChildAt(0)).setTextColor(Color.BLACK);
                switch (addr)
                {
//                    List of open weather station
                    case "Yuktix":   saddr = "\"yuktix1\"";
                        ip ="{\"map\" : {\"module\": \"AWS\" , \"serialNumber\" : "+saddr+" , \"limit\" : \"1\" }}";

                        break;

                    case "Girinagar": saddr="\"ainvvy0\"";
                        ip ="{\"map\" : {\"module\": \"AWS\" , \"serialNumber\" : "+saddr+" , \"limit\" : \"1\" }}";
                        break;

                    case "GKVK Campus": saddr="\"gkvk001\"";
                        ip ="{\"map\" : {\"module\": \"AWS\" , \"serialNumber\" : "+saddr+" , \"limit\" : \"1\" }}";
                        break;

                    case "IISC - Campus": saddr="\"IISC001\"";
                        ip ="{\"map\" : {\"module\": \"AWS\" , \"serialNumber\" : "+saddr+" , \"limit\" : \"1\" }}";
                        break;

                    case "Public Affairs Center, Jigani": saddr="\"pacaws1\"";
                        ip ="{\"map\" : {\"module\": \"AWS\" , \"serialNumber\" : "+saddr+" , \"limit\" : \"1\" }}";
                        break;

                    case "Hebbal (Godrej woodsman)": saddr="\"paws001\"";
                        ip ="{\"map\" : {\"module\": \"AWS\" , \"serialNumber\" : "+saddr+" , \"limit\" : \"1\" }}";
                        break;

                    case "4B Block, Jayanagar": saddr="\"paws002\"";
                        ip ="{\"map\" : {\"module\": \"AWS\" , \"serialNumber\" : "+saddr+" , \"limit\" : \"1\" }}";
                        break;

                    case "Sarjapura (Rainbow Drive)": saddr="\"rainbow\"";
                        ip ="{\"map\" : {\"module\": \"AWS\" , \"serialNumber\" : "+saddr+" , \"limit\" : \"1\" }}";
                        break;

                    case "Indira Nagar (HAL-II)": saddr="\"ramkris\"";
                        ip ="{\"map\" : {\"module\": \"AWS\" , \"serialNumber\" : "+saddr+" , \"limit\" : \"1\" }}";
                        break;

                    case "spoorthi Nagar (Uttarahalli)": saddr="\"sandesh\"";
                        ip ="{\"map\" : {\"module\": \"AWS\" , \"serialNumber\" : "+saddr+" , \"limit\" : \"1\" }}";
                        break;

                    case "Electronics City (Huskur Road)": saddr="\"thejesh\"";
                        ip ="{\"map\" : {\"module\": \"AWS\" , \"serialNumber\" : "+saddr+" , \"limit\" : \"1\" }}";
                        break;

                    case "Vidyaranyapura": saddr="\"zenrain\"";
                        ip ="{\"map\" : {\"module\": \"AWS\" , \"serialNumber\" : "+saddr+" , \"limit\" : \"1\" }}";
                        break;

                    case "IIHS (Sadashiv Nagar)": saddr="\"iihs001\"";
                        ip ="{\"map\" : {\"module\": \"AWS\" , \"serialNumber\" : "+saddr+" , \"limit\" : \"1\" }}";
                        break;

                    case "IIHS (Kengeri campus)": saddr="\"iihs002\"";
                        ip ="{\"map\" : {\"module\": \"AWS\" , \"serialNumber\" : "+saddr+" , \"limit\" : \"1\" }}";
                        break;

                    case "Wipro EC (Weather)": saddr="\"wipro01\"";
                        ip ="{\"map\" : {\"module\": \"AWS\" , \"serialNumber\" : "+saddr+" , \"limit\" : \"1\" }}";
                        break;

                    case "Jakkur Lake": saddr="\"atree01\"";
                        ip ="{\"map\" : {\"module\": \"AWS\" , \"serialNumber\" : "+saddr+" , \"limit\" : \"1\" }}";
                        break;
                }
                textView[0] = (TextView) findViewById(R.id.city_field);
                textView[1] = (TextView) findViewById(R.id.current_temperature_field);
                textView[2] = (TextView) findViewById(R.id.humidity_field);
                textView[3] = (TextView) findViewById(R.id.rain_field);
                textView[4] = (TextView) findViewById(R.id.pressure_field);
                textView[0].setText(addr);
                Function.placeIdTask asyncTask = new Function.placeIdTask(new Function.AsyncResponse() {
                    public void processFinish(ArrayList arrayList) {

                        for (int i = 0; i < arrayList.size(); i++) {
                            textView[i+1].setText((CharSequence) arrayList.get(i));
                        }
                    }
                });
                asyncTask.execute(ip); //The selected Station is sent to a Function and Fetches the requried data.
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }
}
