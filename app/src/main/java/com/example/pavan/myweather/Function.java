package com.example.pavan.myweather;

import android.os.AsyncTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class Function {
    public static final String Urlis ="http://api1.yuktix.com:8080/sensordb/v1/module/device/archive/latest";
    public static final String urlDevice="http://api1.yuktix.com:8080/sensordb/v1//module/devices";

    private static final String Ukey="1868cac0-a92b-4b17-9cef-c540274f9cf0";
    private static String ip;
    private static ArrayList<String> areavalue = new ArrayList<String>();


    public interface AsyncResponse {

        void processFinish(ArrayList arrayList);
    }

    public static class placeIdTask extends AsyncTask<String, String, String> {

        public AsyncResponse delegate = null;//Call back interface

        public placeIdTask(AsyncResponse asyncResponse) {
            delegate = asyncResponse;//Assigning call back interface through constructor
        }

        @Override
        protected String doInBackground(String... params)
        {
        BufferedReader reader = null;
        HttpURLConnection connection=null;


            try {
            URL url = new URL(Urlis);
                ip=params[0];
            connection =(HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setRequestProperty("Authorization","Signature="+Ukey);
            OutputStream os =connection.getOutputStream();
            os.write(ip.getBytes());
            os.flush();
            connection.connect(); // Request  is sent to server (API CALL)
            InputStream stream = connection.getInputStream(); //Fetches the incoming data
            reader = new BufferedReader(new InputStreamReader(stream));
            String line="";
            StringBuffer buffer = new StringBuffer();
                while ((line = reader.readLine())!= null){
                    buffer.append(line);
                }
            String finalJson = buffer.toString();
            JSONObject parentobject = new JSONObject(finalJson);
            /*
            * Seperating the data recived from API and Storing it in separate Variables*/
            JSONArray parentarray = parentobject.getJSONArray("result");
            JSONObject finalobj = parentarray.getJSONObject(0);
            String tempereature = finalobj.getString("T");
            String humidity = finalobj.getString("RH");
            String rain = finalobj.getString("Rain");
            String pressure=finalobj.getString("P");
            Double result = Double.parseDouble(pressure);
            result=result/100;
            areavalue.clear();
            areavalue.add(tempereature+" ºC");
            areavalue.add(humidity+" %");
            areavalue.add(rain+" cm");
            areavalue.add(result+" hPa");

        }catch (MalformedURLException e){
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        } finally {
            if(connection!= null) {
                connection.disconnect();
            }
            try {
                if (reader !=null) {
                    reader.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
            return null;
    }

        @Override
        protected void onPostExecute(String s) {
                     super.onPostExecute(s);
                        delegate.processFinish(areavalue); // Value is returned to Mainactivity
        }
    }

}
